import React, { Component } from 'react';
import { Button, Container, Row, Col, Form } from 'react-bootstrap';
import ReactLoading from 'react-loading';
import '../styles/App.css';
import { getPokemonById } from '../utils/services';

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      number: '',
    };
  }

  handleClick = () => {
    getPokemonById(1)
      .then(res => {
        console.log('Res/Back at app:', res);
        this.setState({
          name: res.data.name,
          number: res.data.national_id,
        });
      })
      .catch(err => {
        console.error('Err/Back to app:', err);
      });
  };

  render() {
    return (
      <div className='app-outer'>
        <Container style={{ backgroundColor: 'yellow' }}>
          <Row>
            <Col>PUT A COMPONENT HERE.</Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col sm={8} style={{ backgroundColor: 'red' }}>
              COMPONENT HERE.
            </Col>
            <Col sm={4} style={{ backgroundColor: 'blue' }}>
              COMPONENT HERE.
            </Col>
          </Row>
          <Row>
            <Col md={{ span: 6, offset: 3 }}>
              <Form>
                <Form.Group controlId='formBasicEmail'>
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type='email' placeholder='Enter email' />
                  <Form.Text className='text-muted'>
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>

                <Form.Group controlId='formBasicPassword'>
                  <Form.Label>Password</Form.Label>
                  <Form.Control type='password' placeholder='Password' />
                </Form.Group>
                <Form.Group controlId='formBasicChecbox'>
                  <Form.Check type='checkbox' label='Check me out' />
                </Form.Group>
                <Button variant='primary' type='submit'>
                  Submit
                </Button>
              </Form>
            </Col>
          </Row>
          <br></br>
        </Container>
        <Button variant='primary' onClick={this.handleClick}>
          Get Pokemon
        </Button>
        <br></br>
        <div style={{ textAlign: 'center' }}>
          <div style={{ display: 'inline-block' }}>
            <ReactLoading
              type={'spinningBubbles'}
              color={'orange'}
              height={30}
              width={30}
            />
          </div>
        </div>
        <p>State:&nbsp;{JSON.stringify(this.state)}</p>
      </div>
    );
  }
}

export default App;
